%global md5() {$(echo -n %1 | md5sum | awk '{print$1}')}

Name:           deepin-wallpapers
Version:        1.8.3
Release:        1
Summary:        Deepin Wallpapers provides wallpapers of dde
License:        GPLv3
URL:            https://github.com/linuxdeepin/deepin-wallpapers
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  dde-api

%description
%{summary}.

%prep
%setup -q -n %{name}-%{version}
sed -i 's|lib|libexec|' Makefile
sed -i 's|lib|libexec|' blur_image.sh

%build
%make_build

%install
install -d %{buildroot}%{_datadir}/wallpapers/deepin/
cp deepin/* %{buildroot}%{_datadir}/wallpapers/deepin/

install -d %{buildroot}%{_var}/cache/
cp -ar image-blur %{buildroot}%{_var}/cache/

install -d %{buildroot}%{_datadir}/backgrounds/
ln -sv ../wallpapers/deepin/desktop.jpg %{buildroot}%{_datadir}/backgrounds/default_background.jpg

%files
%doc README.md
%license LICENSE
%{_datadir}/backgrounds/
%{_datadir}/wallpapers/deepin/
%{_var}/cache/image-blur/

%changelog
* Mon Jul 24 2023 leeffo <liweiganga@uniontech.com> - 1.8.3-1
- upgrade to version 1.8.3

* Thu Jul 21 2022 konglidong <konglidong@uniontech.com> - 1.7.8.1-2
- fix build error

* Tue Jul 19 2022 konglidong <konglidong@uniontech.com> - 1.7.8.1-1
- Update to 1.7.8.1

* Fri Aug 27 2021 konglidong <konglidong@uniontech.com> - 1.6.14-2
- fix build error

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.6.14-1
- Package init
